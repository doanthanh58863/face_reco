from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.models import User

class APIKeyAuthentication(BaseAuthentication):
    def authenticate(self, request):
        api_key = request.headers.get('X-API-KEY')
        if not api_key:
            return None  # No API key header, no authentication

        if api_key != settings.API_KEY:
            raise AuthenticationFailed('Invalid API key')
        user, created = User.objects.get_or_create(username='api_key_user')
        return (user, None)