from django.db import models


# class Bus(models.Model):
#     bus_code = models.CharField(max_length=255)

# Create your models here.
class User(models.Model):
    user_id = models.CharField(max_length=100)
    image_url = models.CharField(max_length=150)
    rekonition_id = models.CharField(max_length=150)
    account_type = models.CharField(max_length=150)
    code = models.CharField(max_length=100)
    branch_id = models.CharField(max_length=100, null=True, blank=True)
    collection = models.CharField(max_length=150)