from email.policy import default
from rest_framework import serializers

from api.models import User

class UserSerializers(serializers.ModelSerializer):

    class Meta:
        fields='__all__'
        model = User


class FaceSaveRequest(serializers.Serializer):
    user_id = serializers.CharField()
    avatar_url = serializers.CharField()
    code = serializers.CharField()
    type = serializers.CharField()
    branch_id = serializers.CharField(default='')
