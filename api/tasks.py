# api/tasks.py

from celery import shared_task
from django.core.mail import send_mail
import boto3
import requests
from urllib.parse import urlparse
from celery.utils.log import get_task_logger
from concurrent.futures import ThreadPoolExecutor, as_completed

from api.models import User

logger = get_task_logger(__name__)

def extract_s3_info(url):
    parsed_url = urlparse(url)
    bucket = parsed_url.netloc.split('.')[0]
    key = parsed_url.path.lstrip('/')
    return {
        "Bucket": bucket,
        "Name": key,
        # "Version": version,  # If you have a version parameter in the URL query string, extract it similarly
    }


def index_face(rekognition_client, collection_id, bucket, key, external_image_id, image_url, user_data):
    try:
        # Check if user_id exists in the User model
        existing_user = User.objects.filter(user_id=user_data['user_id'], collection=collection_id).first()
        updated = False
        logger.error(f"Error indexing image lllllllllll: {external_image_id}")
        
        if existing_user:
            # Remove the face from the collection
            updated = True
            rekognition_client.delete_faces(
                CollectionId=collection_id,
                FaceIds=[existing_user.rekonition_id]
            )
            # Delete the User record
            existing_user.delete()

        # Index the new face
        image_data = requests.get(image_url)
        response = rekognition_client.index_faces(
            CollectionId=collection_id,
            Image={
                'Bytes': image_data.content
            },
            ExternalImageId=external_image_id,
        )


        
        # Save the indexed face details to the User model
        if response and 'FaceRecords' in response:
            face_id = response['FaceRecords'][0]['Face']['FaceId']
            logger.error(f"Error indexing image user_data: {user_data}    {face_id}")
            User.objects.create(
                user_id=user_data['user_id'],
                image_url=user_data['avatar_url'],
                rekonition_id=face_id,
                account_type=user_data['type'],
                code=user_data['code'],
                branch_id=user_data['branch_id'],
                collection=collection_id
            )

        return response, updated
    except Exception as e:
        logger.error(f"Error indexing image {key}: {e}")
        return None, False


@shared_task
def process_faces_and_send_email(valid_data, collection_id, email):
    rekognition_client = boto3.client('rekognition', region_name='ap-southeast-1')
    existing_faces = []
    indexed_faces = []

    try:
        rekognition_client.describe_collection(CollectionId=collection_id)
        logger.info(f"Collection '{collection_id}' already exists.")
    except rekognition_client.exceptions.ResourceNotFoundException:
        try:
            rekognition_client.create_collection(CollectionId=collection_id)
        except Exception as e:
            logger.error(f"Error creating collection: {e}")
            return

    with ThreadPoolExecutor(max_workers=10) as executor:
        future_to_data = {}
        for data in valid_data:
            image_url = data['avatar_url']
            s3_info = extract_s3_info(image_url)
            bucket = s3_info['Bucket']
            key = s3_info['Name']
            u_data = f"{data['user_id']}_{data['code']}_{data['type']}_{data['branch_id']}"
            future = executor.submit(index_face, rekognition_client, collection_id, bucket, key, u_data, image_url, data)
            future_to_data[future] = data

        for future in as_completed(future_to_data):
            data = future_to_data[future]
            try:
                response, updated = future.result()
                
                if updated:
                    existing_faces.append(data)
                else:
                    indexed_faces.append(data)
            except Exception as e:
                logger.error(f"Error processing image {data['avatar_url']}: {e}")

    # Send an email with the results
    try:
        subject = 'Face Processing Results'
        message = f"Updated Faces: {existing_faces}\n\nIndexed Faces: {indexed_faces}"
        send_mail(subject, message, 'doanthanh58863@gmail.com', ['tinh.hoangthi@educo.edu.vn'
                                                                #  'dung.duongtrung@educo.edu.vn'
                                                                 ])
    except Exception as e:
        logger.error(f"Error sending email: {e}")


@shared_task
def test_task():
    logger.info('Test task is running')
    return 'Test task completed'