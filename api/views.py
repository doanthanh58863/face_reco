from django.shortcuts import render
from compreface import CompreFace
from compreface.service import RecognitionService, DetectionService
from compreface.collections import FaceCollection
from compreface.collections.face_collections import Subjects
from rest_framework.views import APIView
from rest_framework.response import Response
import os
from rest_framework import status

from api.models import User
import boto3
import requests
from api.serializers import UserSerializers, FaceSaveRequest
import json
from .tasks import process_faces_and_send_email
from django.conf import settings
from rest_framework.permissions import IsAuthenticated
import logging

logger = logging.getLogger('django')



# Create your views here.


# DOMAIN: str = 'http://103.143.142.245'
# PORT: str = '8880'
# API_KEY: str = '34304fc6-d32b-4c12-993c-f77f69a2429e'

# compareface: CompreFace = CompreFace(DOMAIN, PORT)

# recognition: RecognitionService = compareface.init_face_recognition(API_KEY)
# detection: DetectionService = compareface.init_face_detection('b5380a17-299c-4602-ae13-832fc548c117')
rekognition = boto3.client('rekognition', region_name='ap-southeast-2')
collection_id = 'face_edufit'


class FaceRecognitionView(APIView):
    permission_classes = [IsAuthenticated]

    # def get(self, request):
    #     s3 = boto3.resource('s3')
    #     # rekognition = boto3.client('rekognition', region_name='ap-southeast-2')
    #     # a = s3.buckets.all()
    #     # for bucket in s3.buckets.all():
    #     #     print(bucket.name)
        
    #     try:
    #         response = rekognition.list_collections(MaxResults=1)
    #         collection_ids = response.get('CollectionIds', [])
    #         if len(collection_ids) == 0:
    #             rekognition.create_collection(CollectionId=collection_id)
    #             pass
    #         else:
    #             pass
            
    #     except Exception:
    #         raise
    #     else:
    #         return Response({})

    
    def post(self, request):
        rekognition1 = boto3.client('rekognition', region_name='ap-southeast-1')
        image_file = request.data.get('file', None)
        logger.error(f"============================ {image_file}")
        # file = request.FILES.get('file')
        if image_file is None:
            return Response({'message': 'No file path'}, status=status.HTTP_400_BAD_REQUEST)
        try:
            with image_file.open('rb') as img:
                res = rekognition1.search_faces_by_image(
                    CollectionId=collection_id,
                    Image={
                        'Bytes': img.read(),
                    },
                    MaxFaces=1
                )
                faces = res.get('FaceMatches', [])
                if len(faces) == 0:
                    return Response({'message': 'No user found'}, status=status.HTTP_400_BAD_REQUEST)
                if faces[0].get('Similarity', 0) < 98:
                    return Response({'message': 'No user found'}, status=status.HTTP_400_BAD_REQUEST)
                face = faces[0].get('Face')
                id = face.get('ExternalImageId')
                data = id.split('_')
                json_data = {
                    'user_id': data[0],
                    'code': data[1],
                    'type': data[2],
                    'branch_id': data[3]
                }
                return Response(json_data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'message': 'No face detect'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



class FaceSave(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = FaceSaveRequest(data=request.data, many=True)


        if serializer.is_valid():
            valid_data = serializer.validated_data
            # collection_id = 'your_collection_id'  # Replace with your collection id
            email = 'doanthanh588@gmail.com'  # Replace with the recipient email

            # Trigger the asynchronous task
            process_faces_and_send_email.delay(valid_data, collection_id, email)

            # Return an immediate response
            return Response({'message': 'Request received, processing in background'}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListFacesAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        page = int(request.query_params.get('page', 1))
        pagesize = int(request.query_params.get('pagesize', 10))
        static_url = settings.STATIC_URL  # URL prefix for your static files
        static_root = settings.STATIC_ROOT  # Root directory for your static files

        rekognition_client = boto3.client('rekognition', region_name='ap-southeast-1')
        try:
            rekognition_client.describe_collection(CollectionId=collection_id)
        except rekognition_client.exceptions.ResourceNotFoundException:
            try:
                rekognition_client.create_collection(CollectionId=collection_id)
            except Exception as e:
                return

        faces = []
        next_token = None

        try:
            for _ in range(page):
                if next_token:
                    response = rekognition_client.list_faces(
                        CollectionId=collection_id,
                        MaxResults=pagesize,
                        NextToken=next_token
                    )
                else:
                    response = rekognition_client.list_faces(
                        CollectionId=collection_id,
                        MaxResults=pagesize
                    )

                faces.extend(response.get('Faces', []))
                next_token = response.get('NextToken', None)

                if not next_token:
                    break

            # Only return the faces for the requested page
            start_index = (page - 1) * pagesize
            end_index = start_index + pagesize
            faces = faces[start_index:end_index]

            # Generate URLs for the faces
            face_details = []
            for face in faces:
                external_image_id = face.get('ExternalImageId')
                face_id = face.get('FaceId')

                # Construct the URL for the local file
                data = external_image_id.split('_')
                json_data = {
                    'source_id': data[0],
                    'code': data[1],
                    'type': data[2],
                    'branch_id': data[3],
                    'image_id': face_id,
                }

                face_details.append(json_data)

            result = {
                'faces': face_details,
                'next_token': next_token,
                'page': page,
                'pagesize': pagesize
            }

            return Response(result, status=status.HTTP_200_OK)

        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        
    def delete(self, request, format=None):
        rekognition_client = boto3.client('rekognition', region_name='ap-southeast-1')
        ids = request.data.get('user_ids', [])
        if not ids:
            return Response({"error": "No IDs provided"}, status=status.HTTP_400_BAD_REQUEST)
        deleted_ids = []
        not_found=[]
        for id in ids:
            user = User.objects.filter(collection=collection_id, user_id=id).first()
            if user:
                rekonition_id = user.rekonition_id
                deleted_ids.append(rekonition_id)
            else:
                not_found.append(id)
        
        rekognition_client.delete_faces(
            CollectionId=collection_id,
            FaceIds=deleted_ids
        )
        User.objects.filter(user_id__in=ids).delete()
        results = {
            'not_found': not_found,
            'deleted': deleted_ids,
        }
        return Response(results, status=status.HTTP_200_OK)